<?php
/**
 *
 * @package WordPress
 * @subpackage GTM
 * @since 1.0
 * @version 1.0
 */
get_header(); 

get_template_part( 'template-parts/navigation/page-banner' );
get_template_part( 'template-parts/navigation/breadcrumbs' );

while ( have_posts() ) :
    the_post(); ?>
        <div class="posts__wrapper">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="post__content">
                            <div class="title">
                                <?php echo get_the_post_thumbnail( get_the_ID() ); ?> 
                                <div class="date"><?php echo get_the_date(); ?></div> 
                                <h2><?php the_title(); ?></h2>
                            </div>
                            <div class="text"><?php the_content(); ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile;
get_footer();