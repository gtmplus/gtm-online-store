<?php
/**
 *
 * @package WordPress
 * @subpackage GTM
 * @since 1.0
 * @version 1.0
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>

</head>
<body <?php body_class('background__color');?>>
    <div class="preloader__wrapper">
        <?php if( get_field('logo', 'option') ) { ?>
        <div class="logo__line">
            <img src="<?php the_field('logo', 'option'); ?>" alt="<?php bloginfo( 'name' ); ?>">
        </div>
        <?php } ?>
        <div class="block">
            <div class="spin__loader"></div>
        </div>
    </div>
    <header id="header">
        <div class="top__navigation header__color">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="mobile__menu d-block d-sm-block d-md-block d-lg-none d-xl-none">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                        <?php if( has_nav_menu('main') ) { ?>
                        <div class="additional__menu d-none d-sm-none d-md-none d-lg-block">
                            <?php wp_nav_menu( array(
                                'theme_location'        => 'main',
                                'container'             => 'nav',
                                'container_class'       => 'main__nav'
                            ) ); ?>
                        </div>
                        <?php } ?>
                        <?php if(get_field('logo', 'option')) { ?>
                        <div class="logo__block">
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php the_field('logo', 'option'); ?>" alt="<?php bloginfo( 'name' ); ?>"></a>
                        </div>
                        <?php } ?>
                        <div class="information__block">
                            <?php 
                            if ( function_exists('icl_object_id') ) {
                                $wpml_lang = icl_get_languages();
                                $cur_lang_li = '';
                                $all_lang_li = '';
                                foreach ($wpml_lang as $lang ) {
                                    if($lang['active']){
                                        $cur_lang_li = '<div class="current">
                                                        <img src="'.$lang['country_flag_url'].'" alt="'.$lang['native_name'].'" />
                                                        <span>'.$lang['code'].'</span>
                                                        </div>';
                                    } else {
                                        $all_lang_li .= '
                                                        <li><a href="'.$lang['url'].'" class="wpml_'.$lang['code'].'">
                                                        <img src="'.$lang['country_flag_url'].'" alt="'.$lang['native_name'].'" /><span>'.$lang['code'].'</span></a></li>
                                                        ';
                                    }
                                } ?>
                                <div class="language switcher d-none d-sm-none d-md-none d-lg-block">
                                    <?php echo $cur_lang_li; ?>
                                    <ul>
                                        <?php echo $all_lang_li; ?>
                                    </ul>
                                </div>
                            <?php } 
                            if(get_field('login_page', 'option')) { ?>
                            <div class="login__block d-none d-sm-none d-md-none d-lg-block">
                                <a href="<?php the_field('login_page', 'option'); ?>" class="login__btn">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/login.svg" alt="<?php the_field('login_page_label', 'option'); ?>">
                                    
                                    <?php if( is_user_logged_in() ) { 
                                        $current_user = wp_get_current_user();
                                    ?>
                                        <span><?php echo $current_user->user_login ?></span>
                                    <?php } else { ?>
                                        <span><?php the_field('login_page_label', 'option'); ?></span>
                                    <?php } ?>
                                </a>
                            </div>
                            <?php } ?>
                            <?php if( wc_get_cart_url() ) { ?>
                            <div class="cart__block">
                                <div class="cart__btn">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/cart.svg" alt="Cart">
                                    <?php if( WC()->cart->subtotal ){ ?>
                                    <span><?php echo WC()->cart->subtotal.' '.get_woocommerce_currency_symbol(); ?></span>
                                    <?php } else { ?>
                                        <span>0 <?php echo get_woocommerce_currency_symbol(); ?></span>
                                    <?php } ?>
                                </div>
                                <div class="cart__dropdown">
                                    <?php 
                                    global $woocommerce;
                                    $product_items = $woocommerce->cart->get_cart();
                                    
                                    if( $product_items ) { ?>
                                        <ul>
                                            <?php foreach ( $product_items as $cart_item_key => $cart_item ) {
                                            $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                                            $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
                                                ?>
                                                <li>
                                                    <div class="product__name">
                                                        <h6><?php echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' ); ?></h6>
                                                    </div>
                                                    <div class="product__amount">
                                                        <?php echo $cart_item['quantity']; ?>x
                                                    </div>
                                                    <div class="product__price">
                                                        <?php
                                                            echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
                                                        ?>
                                                    </div>  
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    <?php } else { ?>
                                        <ul>
                                            <li>
                                                <div class="product__name empty"><?php _e('Cart is empty', 'gtm'); ?></div>
                                            </li>
                                        </ul>
                                    <?php } ?>
                                    <a href="<?php echo wc_get_cart_url(); ?>" class="go__to__cart"><?php _e('Go to cart', 'gtm'); ?></a>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="category__navigation d-none d-sm-none d-md-none d-lg-block">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <?php if( has_nav_menu('category-1') || has_nav_menu('category-2') || has_nav_menu('category-3') || has_nav_menu('category-4') ) { ?>
                        <div class="category__menu">
                            <div class="menu">
                                <div class="icon">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                                <div class="name"><?php _e('Product catalog', 'gtm'); ?></div>
                            </div>
                            <div class="categories__nav">
                                <?php 
                                if( has_nav_menu('category-1') ){
                                    wp_nav_menu( array(
                                        'theme_location'        => 'category-1',
                                        'container'             => 'nav',
                                        'container_class'       => 'category__nav'
                                    ) );
                                }
                                if( has_nav_menu('category-2') ){
                                    wp_nav_menu( array(
                                        'theme_location'        => 'category-2',
                                        'container'             => 'nav',
                                        'container_class'       => 'category__nav'
                                    ) );
                                }
                                if( has_nav_menu('category-3') ){
                                    wp_nav_menu( array(
                                        'theme_location'        => 'category-3',
                                        'container'             => 'nav',
                                        'container_class'       => 'category__nav'
                                    ) );
                                }
                                if( has_nav_menu('category-4') ){
                                    wp_nav_menu( array(
                                        'theme_location'        => 'category-4',
                                        'container'             => 'nav',
                                        'container_class'       => 'category__nav'
                                    ) );
                                } ?>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="mobile__navigation d-block d-sm-block d-md-block d-lg-none d-xl-none">
            <div class="menu__wrapper">
                <div class="login__block">
                    <a href="<?php the_field('login_page', 'option'); ?>" class="login__btn">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/login.svg" alt="<?php the_field('login_page_label', 'option'); ?>">
                        <?php if( is_user_logged_in() ) { 
                            $current_user = wp_get_current_user();
                        ?>
                            <span><?php echo $current_user->user_login ?></span>
                        <?php } else { ?>
                            <span><?php the_field('login_page_label', 'option'); ?></span>
                        <?php } ?>
                    </a>
                </div>
                <?php if( has_nav_menu('category-1') || has_nav_menu('category-2') || has_nav_menu('category-3') || has_nav_menu('category-4') ) { ?>
                <div class="category__menu">
                    <h6><?php _e('Categories', 'gtm'); ?></h6>
                    <div class="categories__wrapper">
                        <?php if( has_nav_menu('category-1') ){
                            wp_nav_menu( array(
                                'theme_location'        => 'category-1',
                                'container'             => 'nav',
                                'container_class'       => 'category__nav'
                            ) );
                        }
                        if( has_nav_menu('category-2') ){
                            wp_nav_menu( array(
                                'theme_location'        => 'category-2',
                                'container'             => 'nav',
                                'container_class'       => 'category__nav'
                            ) );
                        }
                        if( has_nav_menu('category-3') ){
                            wp_nav_menu( array(
                                'theme_location'        => 'category-3',
                                'container'             => 'nav',
                                'container_class'       => 'category__nav'
                            ) );
                        }
                        if( has_nav_menu('category-4') ){
                            wp_nav_menu( array(
                                'theme_location'        => 'category-4',
                                'container'             => 'nav',
                                'container_class'       => 'category__nav'
                            ) );
                        } ?>
                    </div>
                </div>
                <?php } ?>
                <?php if( has_nav_menu('main') ) { ?>
                <div class="additional__menu">
                    <?php wp_nav_menu( array(
                        'theme_location'        => 'main',
                        'container'             => 'nav',
                        'container_class'       => 'main__nav'
                    ) ); ?>
                </div>
                <?php } ?>
            </div>
            <div class="additional__bar">
                <?php 
                if ( function_exists('icl_object_id') ) {
                    $wpml_lang = icl_get_languages();
                    $all_lang_li = '';
                    foreach ($wpml_lang as $lang ) {
                        if($lang['active']){
                            $all_lang_li .= '<li class="active"><a href="'.$lang['url'].'" class="wpml_'.$lang['code'].'">
                                            <img src="'.$lang['country_flag_url'].'" alt="'.$lang['native_name'].'" />
                                            <span>'.$lang['code'].'</span></a></li>';
                        } else {
                            $all_lang_li .= '
                                            <li><a href="'.$lang['url'].'" class="wpml_'.$lang['code'].'">
                                            <img src="'.$lang['country_flag_url'].'" alt="'.$lang['native_name'].'" /><span>'.$lang['code'].'</span></a></li>
                                            ';
                        }
                    } ?>
                <div class="language switcher">
                    <ul>
                       <?php echo $all_lang_li; ?>
                    </ul>
                </div>
                <?php } ?>
            </div>
        </div>
    </header>

    <main>