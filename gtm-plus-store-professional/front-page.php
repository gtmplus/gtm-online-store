<?php
/**
 *
 * @package WordPress
 * @subpackage GTM
 * @since 1.0
 * @version 1.0
 */
get_header(); 

if( have_rows('content') ):
    while ( have_rows('content') ) : the_row();
        if( get_row_layout() == 'main_slider' ): 
            get_template_part( 'template-parts/page/main-slider' );
        elseif( get_row_layout() == 'category_menu' ): 
            get_template_part( 'template-parts/page/category-menu' );
        elseif( get_row_layout() == 'partners_slider' ): 
            get_template_part( 'template-parts/page/partners-slider' );
        elseif( get_row_layout() == 'products_section' ): 
            get_template_part( 'template-parts/page/products-section' );
        endif;
    endwhile;
else :
    echo '
        <section class="padding__section">
            <div class="page__content">
                <div class="no__content">
                    <h1>'.__('Nothing to show', 'gtm').'</h1>
                </div>
            </div>
        </section>
    ';
endif;

get_footer();