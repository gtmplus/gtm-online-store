<?php 
$thumbnail = get_the_post_thumbnail_url( get_the_ID() , 'post-thumbnail' ) ? 
			 ' style="background-image:url('.get_the_post_thumbnail_url( get_the_ID() , 'post-thumbnail' ).')"' : ''; 
$excerpt = strtok( wordwrap( get_the_excerpt( get_the_ID() ), 120, "...\n"), "\n");
?>
<div class="col-lg-6">
	<a class="post__block" href="<?php the_permalink(); ?>">
		<div class="thumbnail"<?php echo $thumbnail; ?>></div>
		<div class="details">
			<div class="date"><?php echo get_the_date(); ?></div>
			<h5><b><?php the_title(); ?></b></h5>
			<span class="link"><?php _e('Read more', 'gtm'); ?></span>
		</div>
	</a>
</div>