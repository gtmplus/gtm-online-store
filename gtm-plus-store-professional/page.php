<?php
/**
 *
 * @package WordPress
 * @subpackage GTM
 * @since 1.0
 * @version 1.0
 */
get_header();

get_template_part( 'template-parts/navigation/page-banner' );
get_template_part( 'template-parts/navigation/breadcrumbs' );

if( have_rows('content') ):
    while ( have_rows('content') ) : the_row();
        if( get_row_layout() == 'about_us_section' ): 
            get_template_part( 'template-parts/page/about-us' );
        elseif( get_row_layout() == 'payment_delivery_row' ): 
            get_template_part( 'template-parts/page/payment-delivery' );
        elseif( get_row_layout() == 'content_editor' ): 
            get_template_part( 'template-parts/page/content-editor' );
        elseif( get_row_layout() == 'contact_row' ): 
            get_template_part( 'template-parts/page/contact-row' );
        elseif( get_row_layout() == 'form_map' ): 
            get_template_part( 'template-parts/page/contact-form-and-map' );
        elseif( get_row_layout() == 'icons_row' ): 
            get_template_part( 'template-parts/page/icons-row' );
        elseif( get_row_layout() == 'partners_form' ): 
            get_template_part( 'template-parts/page/partners-form' );
        elseif( get_row_layout() == 'partners_slider' ):
	        get_template_part( 'template-parts/page/partners-slider' );
        elseif( get_row_layout() == 'categories_menu' ):
	        get_template_part( 'template-parts/page/category-menu' );
        elseif( get_row_layout() == 'form_section' ):
            get_template_part( 'template-parts/page/form-section' );
        endif;
    endwhile;
else : ?>
    
        <section class="page__content">
            <div class="container">
                <div class="row">
                    <div class="col"><?php the_content(); ?></div>
                </div>
            </div>
        </section>
<?php endif;

get_footer();