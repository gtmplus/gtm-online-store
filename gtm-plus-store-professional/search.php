<?php
/**
 * Template Name: Search Page
 */

defined( 'ABSPATH' ) || exit;

get_header();

get_template_part( 'template-parts/navigation/page-banner' );
get_template_part( 'template-parts/navigation/breadcrumbs' );

if( !is_search() ){ 
    $category_id = (int) get_queried_object()->term_id;
    $taxonomy = get_queried_object()->taxonomy;
}
global $wp_query;
$total_results = $wp_query->found_posts;

if ( have_posts() ) { ?>
	<div class="products__wrapper">
        <div class="container-fluid">
            <div class="row">
				<?php while ( have_posts() ) { the_post();
						
						do_action( 'woocommerce_shop_loop' );

						wc_get_template_part( 'content', 'product' );
					
				} ?>
			</div>
            <div class="row">
                <div class="col">
                    <?php
                    
                    $total   = isset( $total ) ? $total : $wp_query->max_num_pages;
                    $current = isset( $current ) ? $current : get_query_var('paged');
                    $base    = isset( $base ) ? $base : esc_url_raw( str_replace( 999999999, '%#%', remove_query_arg( 'add-to-cart', get_pagenum_link( 999999999, false ) ) ) );
                    $format  = isset( $format ) ? $format : '';

                    // if ( $total <= 1 ) {
                    // return;
                    // }
                    ?>
                    <nav class="products__pagination">
                    <?php
                        echo paginate_links( apply_filters( 'woocommerce_pagination_args', array( // WPCS: XSS ok.
                            'base'         => $base,
                            'format'       => $format,
                            'add_args'     => false,
                            'current'      => max( 1, $current ),
                            'total'        => $total,
                            'prev_text'    => '&larr;',
                            'next_text'    => '&rarr;',
                            'type'         => 'list',
                            'end_size'     => 3,
                            'mid_size'     => 3,
                        ) ) );
                    ?>
                    </nav>       
                </div>
            </div>
		</div>
	</div>
<?php } else {
	do_action( 'woocommerce_no_products_found' );
}

do_action( 'woocommerce_after_main_content' );

get_footer( 'shop' );
