<?php
/**
 *
 * @package WordPress
 * @subpackage GTM
 * @since 1.0
 * @version 1.0
 */
get_header(); 

get_template_part( 'template-parts/navigation/page-banner' );
get_template_part( 'template-parts/navigation/breadcrumbs' );

?>
    <div class="posts__wrapper">
        <div class="container">
            <div class="row justify-content-center">
                <?php 
                $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                $args = array(
                    'post_type'         => 'post',
                    'post_status'       => 'publish',
                    'paged'             => $paged
                );

                $query = new WP_Query( $args );

                if ( $query->have_posts() ) { ?>
                <div class="col-lg-10">
                    <div class="row">
                    <?php while ( have_posts() ) { the_post();
                        get_template_part( 'template-parts/post/content', 'normal' );
                    } ?>
                    </div>
                </div>
                <?php } wp_reset_postdata(); ?>
            </div>
            <?php 
            if( $query->max_num_pages > 1 ){ ?>
            <div class="row">
                <div class="col">
                    <nav class="products__pagination">
                        <?php echo paginate_links( array(
                            'format'       => 'page/%#%',
                            'current'      => $paged,
                            'total'        => $query->max_num_pages,
                            'prev_text'    => '&larr;',
                            'next_text'    => '&rarr;',
                            'type'         => 'list',
                            'end_size'     => 3,
                            'mid_size'     => 3,
                        )); ?>
                    </nav>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
<?php get_footer();