<?php
/**
 * Register widget areas and widgets
 *
 */
function gtm_widgets_init() {

	register_sidebar( array(
		'name'          => __( 'Catalog Filter', 'gtm' ),
		'id'            => 'catalog-filter',
		'before_widget' => '<div id="%1$s" class="widget catalog-filter-widget %2$s mr-0 mr-md-3 mb-3">',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="widget-title">',
		'after_title'   => '</div>',
	) );

}
add_action( 'widgets_init', 'gtm_widgets_init' );
