<?php 
$background = get_field('page_banner', 'option') ? ' style="background-image: url('.get_field('page_banner', 'option').')"' : '';
$title = '';
if( is_page() ){
    $title = get_the_title();
} elseif ( is_archive() && !is_search() ) {
    $term = get_queried_object();
    $title = $term->name;
} elseif( is_singular('product') ){
    $titles = get_the_terms(get_the_ID(), 'product_cat');
    $title = $titles[0]->name;
} elseif( is_search() ){
    $title = __('Search result for: ', 'gtm').get_search_query();
} elseif( is_home() || is_singular( 'post' ) ) { 
    $blog = get_option( 'page_for_posts' );
    $title = get_the_title( $blog );
} ?>
<div class="container-fluid nopadding">
    <div class="row">
        <div class="page__banner text-center">
        <?php if( get_field('page_banner', 'option') ) { ?><img src="<?php echo get_field('page_banner', 'option'); ?>"><?php } ?>
            <h1><?php echo $title; ?></h1>
        </div>
    </div>
</div>