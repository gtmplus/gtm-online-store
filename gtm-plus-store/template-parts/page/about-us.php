<div class="container">
    <div class="row">
        <div class="col-lg-9">
            <div class="content">
                <?php the_sub_field('content'); ?>
            </div>
        </div>
        <?php if( have_rows('benefits') ): ?>
        <div class="col-lg-3">
            <div class="sidebar">
                <?php while ( have_rows('benefits') ) : the_row(); ?>
                <div class="benefit text-center">
                    <?php if( get_sub_field('icon') ) { ?><img src="<?php the_sub_field('icon'); ?>"><?php } ?>
                    <h6><?php the_sub_field('text'); ?></h6>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>