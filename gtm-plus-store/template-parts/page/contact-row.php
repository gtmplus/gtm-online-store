<?php 
$email = get_sub_field('email');
$phone = get_sub_field('phone');
$address = get_sub_field('address');
?>
<div class="container">
    <div class="row">
        <?php if( $email ) { ?>
        <div class="col-md-12 col-lg-4 col-xl-5">
            <div class="email__block contact">
                <div class="icon__block">
                    <?php if( $email['icon'] ) { ?><img src="<?php echo $email['icon'] ?>" alt="<?php echo $email['title'] ?>"><?php } ?>
                </div>
                <div class="text__block">
                    <?php if( $email['title'] ) { ?><div class="title"><?php echo $email['title'] ?></div><?php } ?>
                    <?php if( $email['email'] ) { ?><a href="mailto:<?php echo $email['email'] ?>"><?php echo $email['email'] ?></a><?php } ?>
                </div>
            </div>
        </div>
        <?php } if( $phone ) { ?>
        <div class="col-md-12 col-lg-4 col-xl-3">
            <div class="phone__block contact">
                <div class="icon__block">
                    <?php if( $phone['icon'] ) { ?><img src="<?php echo $phone['icon'] ?>" alt="<?php echo $phone['title'] ?>"><?php } ?>
                </div>
                <div class="text__block">
                    <?php if( $phone['title'] ) { ?><div class="title"><?php echo $phone['title'] ?></div><?php } ?>
                    <?php if( $phone['phone'] ) { ?><a href="tel:<?php echo $phone['phone'] ?>"><?php echo $phone['phone'] ?></a><?php } ?>
                </div>
            </div>
        </div>
        <?php } if( $address ) { ?>
        <div class="col-md-12 col-lg-4 col-xl-4">
            <div class="address__block contact">
                <div class="icon__block">
                    <?php if( $address['icon'] ) { ?><img src="<?php echo $address['icon'] ?>" alt="<?php echo $address['title'] ?>"><?php } ?>
                </div>
                <div class="text__block">
                    <?php if( $address['title'] ) { ?><div class="title"><?php echo $address['title'] ?></div><?php } ?>
                    <?php if( $address['address'] ) { ?><span><?php echo $address['address'] ?></span><?php } ?>
                </div>
            </div>
        </div>
        <?php  } ?>
    </div>
</div>