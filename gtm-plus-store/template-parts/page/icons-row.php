<div class="container">
    <div class="row">
        <div class="col-lg-4 col-xl-3">
            <div class="partner__benefit">
                <div class="icon">
                    <?php if( get_sub_field('icon_1') ) { ?><img src="<?php the_sub_field('icon_1'); ?>" alt="<?php the_sub_field('title_1'); ?>"><?php } ?>
                </div>
                <?php if( get_sub_field('title_1') ) { ?><h6><?php the_sub_field('title_1'); ?></h6><?php } ?>
            </div>
        </div>
        <div class="col-lg-4 col-xl-3">
            <div class="partner__benefit">
                <div class="icon">
                    <?php if( get_sub_field('icon_1') ) { ?><img src="<?php the_sub_field('icon_2'); ?>" alt="<?php the_sub_field('title_1'); ?>"><?php } ?>
                </div>
                <?php if( get_sub_field('title_2') ) { ?><h6><?php the_sub_field('title_2'); ?></h6><?php } ?>
            </div>
        </div>
        <div class="col-lg-4 col-xl-3">
            <div class="partner__benefit">
                <div class="icon">
                    <?php if( get_sub_field('icon_3') ) { ?><img src="<?php the_sub_field('icon_3'); ?>" alt="<?php the_sub_field('title_3'); ?>"><?php } ?>
                </div>
                <?php if( get_sub_field('title_3') ) { ?><h6><?php the_sub_field('title_3'); ?></h6><?php } ?>
            </div>
        </div>
    </div>
</div>