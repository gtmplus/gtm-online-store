<section class="form__section">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="form__block">
					<?php if( get_sub_field('title') ) { ?>
					<div class="title text-center">
						<h4><?php the_sub_field('title'); ?></h4>
					</div>
					<?php } 
					echo do_shortcode( get_sub_field('form_shortcode') ); ?>
				</div>
			</div>
		</div>
	</div>
</section>