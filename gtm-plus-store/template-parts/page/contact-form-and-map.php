<?php 
$map = get_sub_field('map');
?>
<div class="container">
    <div class="row">
        <div class="col-lg-4">
            <div class="contact__form">
                <?php if( get_sub_field('title') ) { ?><h4><?php the_sub_field('title'); ?></h4><?php } ?>
                <?php if( get_sub_field('contact_form_shortcode') ) echo do_shortcode( get_sub_field('contact_form_shortcode') ); ?>
            </div>
        </div>
        <div class="col-lg-1"></div>
        <div class="col-lg-7">
            <?php if( $map ){ ?>
            <div class="map__block">
                <?php if( $map['title'] ){ ?><h4><?php echo $map['title']; ?></h4><?php } ?>
                <?php if( $map['marker'] && $map['latitude'] && $map['longitude'] && $map['zoom'] ){ ?>
                <div id="google__map" class="map__wrapper" data-lat="<?php echo $map['latitude']; ?>" data-lng="<?php echo $map['longitude']; ?>" data-zoom="<?php echo $map['zoom']; ?>" data-marker="<?php echo $map['marker']; ?>">
                </div>
                <div class="markers-wrapper addresses__block">
                    <a class="marker" data-rel="map-canvas" data-lat="<?php echo $map['latitude']; ?>" data-lng="<?php echo $map['longitude']; ?>" data-str="<p><?php echo $map['map_address']; ?></p>"></a>
                </div>
                <?php } ?>
            </div>
            <?php } ?>
        </div>
    </div>
</div>