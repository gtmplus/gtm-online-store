<?php if( have_rows('rows') ): ?>
<div class="container">
    <?php while ( have_rows('rows') ) : the_row(); ?>
    <div class="section__row">
        <div class="row">
            <div class="col">
                <div class="section__title">
                    <span class="icon">
                        <?php if( get_sub_field('icon') ) { ?><img src="<?php the_sub_field('icon'); ?>"><?php } ?>
                    </span>
                    <?php if( get_sub_field('title') ) { ?><h4><?php the_sub_field('title'); ?></h4><?php } ?>
                </div>
            </div>
        </div>
        <?php if( have_rows('blocks') ): ?>
        <div class="row">
            <?php while ( have_rows('blocks') ) : the_row(); ?>
            <div class="col-sm-6 col-lg-3">
                <div class="delivery">
                <?php if( get_sub_field('text') ) { ?>
                    <div class="text text-center">
                        <?php if( get_sub_field('image') ){ ?>
                        <div class="icon">
                            <img src="<?php the_sub_field('image'); ?>">
                        </div>
                        <?php } ?>
                        <?php the_sub_field('text'); ?>
                    </div>
                <?php } else { ?>
                    <?php if( get_sub_field('image') ){ ?><img src="<?php the_sub_field('image'); ?>"><?php } ?>
                <?php } ?>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
        <?php endif; ?>
    </div>
    <?php endwhile; ?>
</div>
<?php endif; ?>