<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="partners__form">
                <?php if( get_sub_field('title') ) { ?>
                <div class="title__row text-center">
                    <h4><?php the_sub_field('title'); ?></h4>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/form_icon.svg">
                </div>
                <?php } ?>
                <?php if( get_sub_field('form_shortcode') ) echo do_shortcode(get_sub_field('form_shortcode')); ?>
            </div>
        </div>
    </div>
</div>