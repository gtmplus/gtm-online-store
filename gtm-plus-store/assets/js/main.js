(function($) {
    $(document).ready(function(){
        /*Sorting*/
        // $('.sorting a').on('click', function(e){
        //     let sortAttr    = $(this).attr('href').replace('#', ''),
        //         pageUrl     = window.location.href.split("?")[0],
        //         newUrl      = pageUrl + '?orderby=' + sortAttr;
        //     console.log(newUrl);

        //     e.preventDefault();

        // });

        //Preloader scripts
        // $('.preloader__wrapper').addClass('load__done');
        // $('body').removeClass('load');


        /*Mobile menu*/
        $('.mobile__menu').on('click', function(){
            $(this).toggleClass('show__menu');
            $('.mobile__navigation, body').toggleClass('show__navigation');
        });

        /*Mobile menu*/
        $('.mobile__navigation .category__menu h6').on('click', function(){
            $(this).toggleClass('hide__menu');
            $('.mobile__navigation .categories__wrapper').slideToggle();
        });
        /*Main slider*/
        if($('.main__slider').length){
            $('.main__slider').slick({
                infinite:       true,
                autoplay:       true,
                autoplaySpeed:  5000,
                speed:          1000,
                arrows:         false,
                dots:           true,
                fade:           true
            });
        }
        /*Partner slider*/
        if($('.partner__slider').length){
            $('.partner__slider').slick({
                infinite:       true,
                autoplay:       true,
                autoplaySpeed:  5000,
                speed:          800,
                arrows:         false,
                dots:           false,
                slidesToShow:   5,
                responsive: [
                    {
                        breakpoint: 1366,
                        settings: {
                            slidesToShow: 4
                        }
                    },
                    {
                        breakpoint: 1199,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 991,
                        settings: {
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 575,
                        settings: {
                            slidesToShow: 1
                        }
                    }
                ]
            });
        }

        /*Product slider*/
        var $productMainSlider = {
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,    
            arrows: false,
            asNavFor: '.small__slider',
        }
        var $productThumbSlider = {
            infinite: false,
            vertical: true,
            slidesToShow: 3,
            dots: false,
            arrows: false,
            slidesToScroll: 1,
            verticalSwiping: true,
            asNavFor: '.big__slider',
            focusOnSelect: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        vertical: false,
                        verticalSwiping: false,
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 575,
                    settings: {
                        vertical: false,
                        verticalSwiping: false,
                        slidesToShow: 2
                    }
                }
            ]
        }
        $('.big__slider').slick($productMainSlider);
        $('.small__slider').slick($productThumbSlider);

        $('select').select2();
        // $('select').selectric({
        //     arrowButtonMarkup: '<i></i>'
        // });

        if($('.amount__field').length){
            $('.amount__field .amount').on('click', function(){
                var input = $(this).closest('.amount__field').find('input.qty'),
                    field = $(this).closest('.amount__field').find('.number .numer'),
                    max = input.attr('max') ? parseInt(input.attr('max')) : 0,
                    step = parseInt(input.attr('step')),
                    amount = parseInt(input.val());
                if( $(this).hasClass('minus') ){
                    if( amount > 1 ){
                        amount = amount - step;
                        field.text(amount);
                        input.val(amount);
                        $('.update__cart').removeAttr('disabled');
                    }
                } else {
                    if( max == 0 || max > amount ){
                        amount = amount + step;
                        field.text(amount);
                        input.val(amount);
                        $('.update__cart').removeAttr('disabled');
                    }
                }
            });
        }
        
        /*Google map*/
        $(function() {
            var marker = [],
                infowindow = [],
                map, image = $('.map__wrapper').attr('data-marker');

            function addMarker(location, name, contentstr) {
                marker[name] = new google.maps.Marker({
                    position: location,
                    map: map,
                    icon: image
                });
                marker[name].setMap(map);

                infowindow[name] = new google.maps.InfoWindow({
                    content: contentstr
                });

                google.maps.event.addListener(marker[name], 'click', function() {
                    infowindow[name].open(map, marker[name]);
                });
            }

            function initialize() {
                var lat = $('.map__wrapper').attr("data-lat");
                var lng = $('.map__wrapper').attr("data-lng");
                var mark_name = $('.map__wrapper').attr("data-name");
                var mark_str = $('.map__wrapper').attr("data-str");

                var myLatlng = new google.maps.LatLng(lat, lng);

                var setZoom = parseInt($('.map__wrapper').attr("data-zoom"));

                var styles = [
                    {
                        "featureType": "water",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#e9e9e9"
                            },
                            {
                                "lightness": 17
                            }
                        ]
                    },
                    {
                        "featureType": "landscape",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#f5f5f5"
                            },
                            {
                                "lightness": 20
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            },
                            {
                                "lightness": 17
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            },
                            {
                                "lightness": 29
                            },
                            {
                                "weight": 0.2
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            },
                            {
                                "lightness": 18
                            }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            },
                            {
                                "lightness": 16
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#f5f5f5"
                            },
                            {
                                "lightness": 21
                            }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#dedede"
                            },
                            {
                                "lightness": 21
                            }
                        ]
                    },
                    {
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "visibility": "on"
                            },
                            {
                                "color": "#ffffff"
                            },
                            {
                                "lightness": 16
                            }
                        ]
                    },
                    {
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "saturation": 36
                            },
                            {
                                "color": "#333333"
                            },
                            {
                                "lightness": 40
                            }
                        ]
                    },
                    {
                        "elementType": "labels.icon",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "transit",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#f2f2f2"
                            },
                            {
                                "lightness": 19
                            }
                        ]
                    },
                    {
                        "featureType": "administrative",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#fefefe"
                            },
                            {
                                "lightness": 20
                            }
                        ]
                    },
                    {
                        "featureType": "administrative",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "color": "#fefefe"
                            },
                            {
                                "lightness": 17
                            },
                            {
                                "weight": 1.2
                            }
                        ]
                    }
                ];

                var styledMap = new google.maps.StyledMapType(styles, { name: "Styled Map" });

                var mapOptions = {
                    zoom: setZoom,
                    disableDefaultUI: false,
                    scrollwheel: false,
                    zoomControl: true,
                    streetViewControl: true,
                    center: myLatlng
                };
                map = new google.maps.Map(document.getElementById("google__map"), mapOptions);

                map.mapTypes.set('map_style', styledMap);
                map.setMapTypeId('map_style');

                $('.addresses__block a').each(function() {
                    var mark_lat = $(this).attr('data-lat');
                    var mark_lng = $(this).attr('data-lng');
                    var this_index = $('.addresses-block a').index(this);
                    var mark_name = 'template_marker_' + this_index;
                    var mark_locat = new google.maps.LatLng(mark_lat, mark_lng);
                    var mark_str = $(this).attr('data-str');
                    addMarker(mark_locat, mark_name, mark_str);
                });

            }

            if ($('.map__wrapper').length) {
                setTimeout(function() {
                    initialize();
                }, 500);
            }
        });

        // function getCities(city){
        //     let settings = {
        //         type: "POST",
        //         dataType: "json",
        //         url: "https://api.novaposhta.ua/v2.0/json/",
        //         data: JSON.stringify({
        //             "apiKey": apiKey,
        //             "modelName": "Address",
        //             "calledMethod": "getCities",
        //             "methodProperties": {
        //                 "FindByString": city
        //             }
        //         }),
        //         headers: {
        //             "Content-Type": "application/json"
        //         },
        //         xhrFields: { 
        //             withCredentials: false 
        //         }
        //     };
        //     $.ajax(settings).done(function (response) { 
        //         let cities = response.data,
        //             citiesField = $('#city'),
        //             citiesHtml = '',
        //             citiesData = [];

        //         cities.forEach(function(city){
        //             if ( citiesField.find("option[value='" + city.Ref + "']").length ) {
                        
        //             } else { 
        //                 var newOption = new Option(city.Description, city.Ref, false, false);
        //                 citiesData.push(
        //                     {
        //                         id: city.Ref,
        //                         text: city.Description
        //                     }
        //                 );
        //                 citiesField.append(newOption);
        //             }
        //         });
        //         $('#city').select2({
        //             data: citiesData
        //         });
        //     });

        // }

        // function getWarehouse(ref){
        //     let settings = {
        //         type: "POST",
        //         dataType: "json",
        //         url: "https://api.novaposhta.ua/v2.0/json/",
        //         data: JSON.stringify({
        //             "apiKey": apiKey,
        //             "modelName": "AddressGeneral",
        //             "calledMethod": "getWarehouses",
        //             "methodProperties": {
        //                 "SettlementRef": ref
        //             }
        //         }),
        //         headers: {
        //             "Content-Type": "application/json"
        //         },
        //         xhrFields: { 
        //             withCredentials: false 
        //         }
        //     };
        //     $.ajax(settings).done(function (response) { 
        //         let wareHouses = response.data,
        //             wareHousesHtml = '';
                    
        //         wareHouses.forEach(function(warehouse){
        //             wareHousesHtml += '<option value="'+ warehouse.Description +'">'+ warehouse.Description +'</option>';
        //         });
        //         $('#warehouse').append(wareHousesHtml).selectric();
        //     });
        // }

        /*Woocommerce shiping*/
        // var country = $('#country'),
        //     countryVal = country.val(),
        //     otherCountry = $('#other_country_field input'),
        //     delivery = $('#delivery_method'),
        //     region = $('#region'),
        //     cities = $('#city'),
        //     warehoses = $('#warehouse');

        // var novaPoshtaRegion = $('#billing_nova_poshta_region'),
        //     novaPoshtaCity = $('#billing_nova_poshta_city'),
        //     novaPoshta = $('#billing_nova_poshta_warehouse');

        // novaPoshtaRegion.on('change', function(){
        //     setTimeout(function() {
        //         novaPoshtaCity.selectric();
        //     }, 500);
            
        // });

            
        // country.on('change', function(){
        //     countryVal = country.val();
        //     console.log(countryVal);
        //     if( countryVal === 'ua' ){
        //         $('#delivery_method_field').removeClass('hide__field');
        //         $('#other_country_field').addClass('hide__field');
        //     } else if( countryVal === 'others' ){
        //         $('#other_country_field').removeClass('hide__field');
        //         $('#delivery_method_field').addClass('hide__field');
        //     } else if( countryVal == '' ){
        //         $('#other_country_field').addClass('hide__field');
        //         $('#delivery_method_field').addClass('hide__field');
        //     }
        // });

        // delivery.on('change', function(){
        //     let deliveryVal = $(this).val();

        //     if(deliveryVal === 'nova'){
        //         $('#region_field').removeClass('hide__field');
        //         $('#other_delivery_field').addClass('hide__field');
        //     } else if( deliveryVal === 'others' ) {
        //         $('#region_field').addClass('hide__field');
        //         $('#other_delivery_field').removeClass('hide__field');
        //     }
        // });

        // region.on('change', function(){
        //     let regionVal = region.val();
            
        //     if(regionVal != ''){
        //         $('#city_field').removeClass('hide__field');
        //     } else {
        //         $('#city_field').addClass('hide__field');
        //     }
        // });

    });
})(jQuery);