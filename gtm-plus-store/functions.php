<?php
/**
 *
 * @package WordPress
 * @subpackage GTM
 * @since 1.0
 */

/*Theme setup*/
function gtm_setup() {
    load_theme_textdomain( 'gtm' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'woocommerce' );

    add_image_size( 'product-small', 160, 185, false );
    add_image_size( 'product-thumbnail', 350, 350, false );

    register_nav_menus( array(
        'main'          => __( 'Main Menu', 'gtm' ),
        'category-1'    => __( 'Category Menu 1', 'gtm' ),
        'category-2'    => __( 'Category Menu 2', 'gtm' ),
        'category-3'    => __( 'Category Menu 3', 'gtm' ),
        'category-4'    => __( 'Category Menu 4', 'gtm' )
    ) );

	// Register sidebars
	locate_template( '/inc/sidebars.php', true );
}
add_action( 'after_setup_theme', 'gtm_setup' );

/*App2drive styles and scripts*/
function gtm_scripts() {
    $version = '1.0.76';

    wp_enqueue_style( 'gtm-css', get_theme_file_uri( '/assets/css/main.min.css' ), '', $version );
    wp_enqueue_style( 'gtm-style', get_stylesheet_uri() );

    if( get_field('menu_icons', 'options') ){
        $icons_array = get_field('menu_icons', 'options');
        $icons_css = '';
        foreach ( $icons_array as $icon ) {
            
            $icons_css .= '.category__menu li'.$icon['item_class'].'>a::before{background-image: url('.$icon['item_icon']['url'].');}';
        }
        wp_add_inline_style( 'gtm-css', $icons_css );
    }

    wp_enqueue_script( 'google-maps-key', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDOqauekT_r4JzPY18FOWN4N8DFR3hXo1U&libraries=places', array( 'jquery' ), $version, true );
    wp_enqueue_script( 'share-js', '//platform-api.sharethis.com/js/sharethis.js#property=5b77cc47f8352a0011896bd5&product=custom-share-buttons', array('jquery'), $version, true );
    wp_enqueue_script( 'all-js', get_theme_file_uri( '/assets/js/all.min.js'), array('jquery'), $version, true );
}
add_action( 'wp_enqueue_scripts', 'gtm_scripts' );

/*Theme settings*/
if( function_exists('acf_add_options_page') ) {

    $general = acf_add_options_page(array(
        'page_title'    => 'Theme General Settings',
        'menu_title'    => 'Theme Settings',
        'redirect'      => false,
        'capability'    => 'edit_posts',
        'menu_slug'     => 'theme-settings',
    ));
}

function gtm_styles() {
    $style_css = '';

    /*Blocks Colors*/
    $header_color = get_field('header_color', 'option');
    $background_color = get_field('background_color', 'option');
    $accent_color = get_field('accent_color', 'option');
    $footer_color = get_field('footer_color', 'option');
    $heading_color = get_field('headings_color', 'option');
    $paragraph_color = get_field('paragraph_color', 'option');
    $link_color = get_field('link_color', 'option');
    $link_hover_color = get_field('link_hover_color', 'option');

    if( $header_color ) $style_css .= '.header__color{ background: '.$header_color.'!important;}';

    if( $background_color ) $style_css .= '.background__color{ background: '.$background_color.'!important;}';

    if( $accent_color ) $style_css .= '
        .preloader__wrapper .block .spin__loader{
            border-top: 4px solid '.$accent_color.'!important;
        }
        ::-webkit-scrollbar-thumb,
        .accent__color, 
        .btn.red__btn,
        .button.red__btn,
        .btn.to__cart.red,
        .btn.to__cart.red .layout,
        .button.to__cart.red,
        .button.to__cart.red .layout,
        .btn.to__cart::after,
        .btn.to__cart::before,
        .button.to__cart::after,
        .button.to__cart::before,
        .btn.red__btn .layout,
        .button.red__btn .layout,
        .main__slider .slick-dots li.slick-active button,
        .suscribe__line form button,
        .products__pagination ul li span,
        .cart__section .cart__item td.remove a:hover,
        .cart__block .cart__dropdown .go__to__cart,
        .woocommerce-checkout #payment [type=radio]:checked+label:after,
        .search__block form button,
        .select2-dropdown .select2-results .select2-results__option.select2-results__option--highlighted, 
        .select2-dropdown .select2-results .select2-results__option:hover{
            background: '.$accent_color.'!important;
        }
        .product__block:hover,
        .products__pagination ul li span,
        .products__pagination ul li a:hover,
        .btn.simple:hover,
        .button.simple:hover,
        .category__menu .menu .icon span,
        .form__block, 
        .partners__form{
            border-color: '.$accent_color.'!important;
        }
        .page__banner h1 span,
        h3.section__title span,
        .btn.red__btn:hover .text, 
        .button.red__btn:hover .text,
        .category__menu li a:hover,
        .category__menu li.current-menu-item a,
        .contact__block .content__block a:hover,
        .btn.simple:hover,
        .button.simple:hover,
        .category__menu:hover .menu .name{
            color: '.$accent_color.'!important;
        }
        .cart__block .cart__dropdown{
            border-top: 2px solid '.$accent_color.'!important;
        }
        .cart__block .cart__dropdown::before{
            border-color: transparent transparent '.$accent_color.'!important;
        }
    ';
    if( $footer_color ) $style_css .= '.footer__color{ background: '.$footer_color.'!important;}';

    if( $heading_color ) $style_css .= '
        body,
        .main__slider .slide .text__block h4,
        .main__slider .slide .text__block h3,
        .category__banner h5,
        .product__block h5,
        h3.section__title,
        .contact__block .content__block .title,
        .section__title h4,
        .page__banner h1,
        .product__information h2,
        .product__information p.pric,
        .sidebar .benefit h6,
        .delivery .text h6,
        .payment .text h6,
        .delivery .text p,
        .payment .text p,
        .contact .text__block a,
        .contact .text__block span,
        .woocommerce table.shop_table th,
        .fill__form .title h6,
        h6#order_review_heading,
        .woocommerce form .form-row label{
            color: '.$heading_color.'!important;
        }
    ';

    if( $paragraph_color ) $style_css .= '
        p,
        .login__block a,
        .switcher .current span,
        .switcher ul li a,
        .product__information .article,
        .cart__block .cart__dropdown ul li .product__name h6,
        .content p,
        .contact .text__block .title,
        .field__group input, 
        .field__group textarea,
        .squared__btn,
        .woocommerce form.checkout input,
        .woocommerce form.checkout_coupon input,
        .woocommerce form.login input,
        .woocommerce form.lost_reset_password input,
        .woocommerce form.register input{
            color: '.$paragraph_color.';
        }
    ';

    if( $link_color ) $style_css .= '
        a,
        .woocommerce-MyAccount-navigation ul li a,
        .cart__section .cart__item .information a,
        .category__banner .link,
        .additional__menu li a{
            color: '.$link_color.';
        }
    ';

    if( $link_hover_color ) $style_css .= '
        a:hover,
        .woocommerce-MyAccount-navigation ul li.is-active a,
        .woocommerce table.my_account_orders .button,
        .cart__section .cart__item .information a:hover,
        .woocommerce-error a,
        .woocommerce-info a,
        .woocommerce-message a,
        .category__banner:hover .link,
        .additional__menu li a:hover,
        .woocommerce-MyAccount-navigation ul li a:hover,
        .post__block .details span.link:hover,
        .post__block:hover span.link{
            color: '.$link_hover_color.';
        }
        .category__banner:hover .link::before{
            background-color: '.$link_hover_color.';
        }
    ';

    wp_add_inline_style( 'gtm-css', $style_css );
}

add_action( 'wp_enqueue_scripts', 'gtm_styles' );


/*SVG support*/
function gtm_svg_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'gtm_svg_types');

/*App2drive ajax*/
function gtm_ajaxurl() {
?>
    <script type="text/javascript">
        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
        <?php if( get_field( 'api_key', 'option' ) ) { ?>
            var apiKey = '<?php the_field( 'api_key', 'option' ); ?>';
        <?php } ?>
    </script>
<?php
}
add_action('wp_footer', 'gtm_ajaxurl');

/*Change currency symbol*/
// add_filter('woocommerce_currency_symbol', 'change_existing_currency_symbol', 10, 2);

// function change_existing_currency_symbol( $currency_symbol, $currency ) {
//     $current_lang = ICL_LANGUAGE_CODE;

//     if($current_lang == 'uk'){
//         $symbol = ' грн.';
//     } elseif ($current_lang == 'ru') {
//         $symbol = ' грн.';
//     } else {
//         $symbol = ' UAH';
//     }

//     switch( $currency ) {
//         case 'UAH': $currency_symbol = $symbol; break;
//     }
//     return $currency_symbol;
// }

function gtm_no_limit_posts( $query ) {
    if( is_archive() ){
        $query->set( 'posts_per_page', 12 );
    }
}
add_action( 'pre_get_posts', 'gtm_no_limit_posts' );

// /*Remove billing fields*/
// add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
 
// function custom_override_checkout_fields( $fields ) {
//     unset($fields['billing']['billing_first_name']);
//     unset($fields['billing']['billing_last_name']);
//     unset($fields['billing']['billing_email']);
//     unset($fields['billing']['billing_phone']);
//     unset($fields['billing']['billing_address_1']);
//     unset($fields['billing']['billing_address_2']);
//     unset($fields['billing']['billing_state']);
//     unset($fields['billing']['billing_postcode']);
//     unset($fields['billing']['billing_city']);
//     unset($fields['billing']['billing_company']);
//     return $fields;
// }
// add_filter('woocommerce_checkout_fields', 'custom_woocommerce_billing_fields');

// function custom_woocommerce_billing_fields($fields) {
//     $fields['billing']['billing_first_name'] = array(
//         'label' => '',
//         'placeholder' => YOURFIRSTNAME,
//         'required' => true,
//         'clear' => false,
//         'type' => 'text',
//         'class' => array('field__group')
//     );

//     $fields['billing']['billing_last_name'] = array(
//         'label' => '',
//         'placeholder' => YOURLASTNAME,
//         'required' => true,
//         'clear' => false,
//         'type' => 'text',
//         'class' => array('field__group')
//     );

//     $fields['billing']['billing_phone'] = array(
//         'label' => '',
//         'placeholder' => YOURPHONE,
//         'required' => true,
//         'clear' => false,
//         'type' => 'tel',
//         'class' => array('field__group')
//     );

//     $fields['billing']['billing_email'] = array(
//         'label' => '',
//         'placeholder' => YOUREMAIL,
//         'required' => true,
//         'clear' => false,
//         'type' => 'email',
//         'class' => array('field__group')
//     );

//     $fields['billing']['other_delivery'] = array(
//         'label' => '',
//         'placeholder' => FILLDELIVERY,
//         'required' => false,
//         'clear' => true,
//         'type' => 'textarea',
//         'class' => array('field__group')
//     );

//     return $fields;
// }

// add_action( 'woocommerce_checkout_update_order_meta', 'bsw_custom_checkout_field_update_order_meta' );

// function bsw_custom_checkout_field_update_order_meta( $order_id ) {
//     if ( ! empty( $_POST['other_delivery'] ) ) {
//         update_post_meta( $order_id, 'other_delivery', sanitize_text_field( $_POST['other_delivery'] ) );
//     }
// }

// add_action( 'woocommerce_admin_order_data_after_billing_address', 'bsw_checkout_field_display_admin_order_meta', 10, 1 );

// function bsw_checkout_field_display_admin_order_meta($order){
//     if( get_post_meta( $order->id, 'other_delivery', true ) ) echo '<p><strong>'.__('Other delivery type information').':</strong> ' . get_post_meta( $order->id, 'other_delivery', true ) . '</p>';
// }


//Product filters checker
function is_product_filter_exist($GET_arr){
	if ( isset( $GET_arr['orderby'] ) ) {
		return true;
	}

    foreach ($GET_arr as $key => $GET_arr_value){
        $is_filter = strpos( $key, 'filter' );
        if( $is_filter !== false ){
	        return true;
        }
    }

    return false;
}

add_filter('acf/settings/load_json', 'parent_theme_field_groups');
function parent_theme_field_groups($paths) {
	$path = get_template_directory().'/acf-json';
	$paths[] = $path;
	return $paths;
}

add_filter( 'wp_postratings_ratings_image_alt', 'wp_postratings_ratings_image_alt' );
function wp_postratings_ratings_image_alt( $alt_title_text ) {
	return '';
}