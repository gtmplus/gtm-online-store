<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<a href="<?php echo esc_url( wc_get_checkout_url() );?>" class="red__btn btn alt">
	<div class="layout"></div>
    <span class="text"><?php esc_html_e( 'Proceed to checkout', 'woocommerce' ); ?></span>
</a>
