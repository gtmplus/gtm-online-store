<?php
global $product;

defined( 'ABSPATH' ) || exit;

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
$product_image = get_the_post_thumbnail_url($post->ID);
$product_thumb = get_the_post_thumbnail_url($post->ID, 'product-thumbnail');
$attachment_ids = $product->get_gallery_image_ids();

?>
<div class="products__wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="product__notice">
                    <?php do_action( 'woocommerce_before_single_product' ); ?>
                </div>  
				<div id="product-<?php the_ID(); ?>" <?php wc_product_class(); ?>>
					<div class="product__image">
                        <div class="big__slider">
                        	<?php if( $product_image ) { ?>
                            <div class="slide">
                                <div class="image">
                                    <img src="<?php echo $product_image; ?>" alt="<?php the_title(); ?>">
                                </div>
                            </div>
                        	<?php } 
                        	if( $attachment_ids ){
                        		foreach ($attachment_ids as $attachment) { 
                        			$image = wp_get_attachment_image_src( $attachment, 'full' );
                        			?>
	                        		<div class="slide">
		                                <div class="image">
		                                    <img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>">
		                                </div>
		                            </div>
                        		<?php }
                        	} ?>
                        </div>
                        <div class="small__slider">
                            <?php if( $product_thumb ) { ?>
                            <div class="slide">
                                <div class="image">
                                    <img src="<?php echo $product_thumb; ?>" alt="<?php the_title(); ?>">
                                </div>
                            </div>
                        	<?php } 
                        	if( $attachment_ids ){
                        		foreach ($attachment_ids as $attachment) { 
                        			$image = wp_get_attachment_image_src( $attachment, 'product-thumbnail' );
                        			?>
		                            <div class="slide">
		                                <div class="image">
		                                    <img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>">
		                                </div>
		                            </div>
                        		<?php }
                        	} ?>
                        </div>
                    </div>
					
                    <div class="product__information">
                        <div class="title__row">
                            <h2><?php the_title(); ?></h2>
                            <?php 
                            $manage_stock = $product->managing_stock();
                            $on_stock = $product->is_in_stock();
                            
                            if( $manage_stock ) { 
                            	if ( $on_stock ) { ?>
                            		<span class="status on__stock"><?php _e('In stock', 'gtm'); ?></span>
                            	<?php } else { ?>
                            		<span class="status out__stock"><?php _e('Out of stock', 'gtm'); ?></span>
                            	<?php }
                            } ?>
                        </div>
						<div class="article"><?php _e('SKU: ', 'gtm'); ?><?php echo $product->get_sku(); ?></div>
                        <div class="product-rating">
		                    <?php if(function_exists('the_ratings')) { the_ratings(); } ?>
                        </div>
						<div class="description"><?php the_content(); ?></div>
                        <div class="product__form">
                            <div class="variations__block">
                                <div class="button__block">
                                    <?php 
                                        woocommerce_template_single_add_to_cart(); 
                                    ?>
                                </div>
                                <?php woocommerce_template_single_price(); ?>
                            </div>
                        </div>
                        <div class="social__menu">
                            <span class="icon"></span>
                            <ul class="social__links">
                                <li>
                                    <span class="facebook st-custom-button" data-network="facebook" data-short-url="<?php the_permalink();?>" data-title="<?php the_title(); ?>" data-description="<?php echo strip_tags(get_the_content()); ?>" data-image="<?php echo get_the_post_thumbnail_url(); ?>">
                                    </span>
                                </li>
                                <li>
                                    <span class="twitter st-custom-button" data-network="twitter" data-short-url="<?php the_permalink();?>" data-title="<?php the_title(); ?>" data-description="<?php echo strip_tags(get_the_content()); ?>" data-image="<?php echo get_the_post_thumbnail_url(); ?>">
                                    </span>
                                </li>
                            </ul>
                        </div>
					</div>
				</div>
			</div>
		</div>

        <?php woocommerce_output_related_products(); ?>
	</div>
</div>
<?php do_action( 'woocommerce_after_single_product' ); ?>